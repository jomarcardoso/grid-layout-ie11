# GRID LAYOUT IE11
A couple of grid-system SASS mixins to IE11 to be ```@included``` as you need

Just copy the **[code](https://github.com/jomarchumi/grid-layout-IE11/blob/master/src/sass/common/mixins/_grid-layout-mixins.scss)** and add to your project.

**[Documentation](https://github.com/jomarchumi/grid-layout-IE11/wiki/Documentation)**

The purpose of this library is to facilitate the use of the Grid-layout on IE11.

[https://caniuse.com/#feat=css-grid](https://caniuse.com/#feat=css-grid)

Grid-layout can be very useful in more sophisticated layouts that break the block pattern and responsive layouts that change the order of the elements.
In IE the technology is much more limited so we can use the following mixes compatible with IE.


```
/*
* Grid-Layout-retrocompatible - v1.2.1
* @jomarcardoso
*/

@mixin grid() {
    display: -ms-grid;
    display: grid;
}

@mixin grid-gap($gap) {
    margin: #{-$gap / 2};
    & > * {
        margin: #{$gap / 2};
    }
}

@mixin grid-column($column, $span) {
    -ms-grid-column: $column;
    -ms-grid-column-span: $span;
    grid-column: $column / span $span;
}

@mixin grid-row($row, $span) {
    -ms-grid-row: $row;
    -ms-grid-row-span: $span;
    grid-row: $row / span $span;
}

@mixin grid-template-columns($list) {
    $value: null;
    $-ms-value: null;
    $cell: 0;
    @for $i from 1 through length($list) {
        $cell: $cell + nth($list, $i);
    }
    $percent-size: 100 / $cell;
    $percent: '%';
    @for $i from 1 through length($list) {
        $value: $value #{nth($list, $i)}fr;
        $-ms-value: $-ms-value #{nth($list, $i) * $percent-size }#{$percent};
    }
    -ms-grid-columns: $-ms-value;
    grid-template-columns: $value;
}

/*
* param: number of columns wanted
* return:   -ms-grid-columns: [list of columns];
*           grid-template-columns: [list of columns];
*/
@mixin grid-columns($columns) {
    $value: null;
    $-ms-value: null;
    $percent-size: 100 / $columns;
    $percent: '%';
    @for $i from 1 through $columns {
        $value: $value 1fr;
        $-ms-value: $-ms-value #{1 * $percent-size }#{$percent};
    }
    -ms-grid-columns: $-ms-value;
    grid-template-columns: $value;
}

@mixin grid-template-rows($list) {
    $value: null;
    $-ms-value: null;
    $cell: 0;
    @for $i from 1 through length($list) {
        $cell: $cell + nth($list, $i);
    }
    $percent-size: 100 / $cell;
    $percent: '%';
    @for $i from 1 through length($list) {
        $value: $value #{nth($list, $i)}fr;
        $-ms-value: $-ms-value #{nth($list, $i)}fr;
    }
    -ms-grid-rows: $-ms-value;
    grid-template-rows: $value;
}

@mixin grid-rows($rows) {
    $value: null;
    $-ms-value: null;
    $percent-size: 100 / $rows;
    $percent: '%';
    @for $i from 1 through $rows {
        $value: $value 1fr;
        $-ms-value: $-ms-value #{1 * $percent-size }#{$percent};
    }
    -ms-grid-rows: $-ms-value;
    grid-template-rows: $value;
}
```

## Usage example

**HTML:**
```
<section class="grid">
    <div class="grid-item grid-item-a">A</div>
    <div class="grid-item grid-item-b">B</div>
    <div class="grid-item grid-item-c">C</div>
    <div class="grid-item grid-item-d">D</div>
</section>
```

**CSS:**
```
.grid {
    display: grid;
    @include grid-columns-size((6, 3, 3, 2, 2, 2));
    @include grid-rows-size((6, 2));

    &-item {

        &-a {
            @include grid-column(1, 1);
            @include grid-row(1, 1);
        }

        &-b {
            @include grid-column(2, 2);
            @include grid-row(1, 1);
        }

        &-c {
            @include grid-column(4, 3);
            @include grid-row(1, 1);
        }

        &-d {
            @include grid-column(1, 6);
            @include grid-row(2, 1);
        }
    }
}
```

**Result:**

![result-image](files/grid-layout-example.jpg)

# GRID-LAYOUT 

Prefere-se usar "Grid" quando:

- O layout foge do padrão de "blocos", exemplo:
```
   ___ ___ ___   _____
  |   |   |   | |     |
  |___|___|___| |     |
 _____   _____  |     |
|     | |_____| |_____|
|     |  ___ ___ ___
|     | |   |   |   |
|_____| |___|___|___| 
```

- Quando o reponsivo muda a ordem dos elementos em tela. 
_Com grid pode-se escolher onde cada elemento vai e também se necessário deixar espaços vazios._
Exemplo, o layout em "desktop" de cima como ficou no "mobile":
```
 _____
|     |
|     |
|     |
|_____|
 __ __
|  |  |
|__|__|
 _____
|_____|
...
```

...Em geral qualquer mudança na ordem dos elemtos pode ser útil "Grid".

## Grid-layout em novos navegadores
A forma mais fácil e "eficiente" de usar "grid" é com ```grid-area``` + ```grid-template-areas```, exemplo:

**HTML:**
```
<section class="grid">
  <div class="grid-item-a">A</div>
  <div class="grid-item-b">B</div>
  <div class="grid-item-c">C</div>
</section>
```

**css:**
```
.grid {
  display: grid;
  grid-template-areas: 
    'a . c'
    'b b b';
  grid-template-columns: 1fr 2fr 1fr;
  grid-template-rows: 3fr 2fr;
}

.grid-item-a {
  grid-area: a;
}

.grid-item-b {
  grid-area: b;
}

.grid-item-c {
  grid-area: c;
}
```

**resultado**:
```
 ____          ____
|    |        |    |
| A  |        | C  |
|____|________|____|
|         B        |
|__________________|
 
```
Há várias outras propriedades, uma muito usada é a ```grid-gap:``` para escolher o espaçamentos interno entre os elementos.
**OBS:** o "." representa espaço vazio.
**Atenção:** um erro muito comum é usar o nome do ```grid-area``` entre aspas.

## Particularidades do IE
No Internet Explorer funciona apenas na versão 11, com muitas limitações e sempre com o prefixo ```-ms-```.
Das pricipais faltas no IE é a "forma fácil" que mencionada acima, não há ```grid-area``` + ```grid-template-areas```,
isso tira a possibilidade do "pai" escolher a posição dos "filhos" e passa para eles mesmo definirem isso.

O que sobra é a forma mais antiga de grid onde o "pai" controla apenas:
- largura das colunas:
  ```-ms-grid-columns: 33.333333% 66.666666% 33.333333%;``` para IE.
  ```grid-template-columns: 1fr 2fr 1fr;``` para demais navegadores.
- algura das linhas:
  ```-ms-grid-rows: 3fr 2fr;``` aqui o "fr" pode ser usado para IE.
  ```grid-template-rows: 3fr 2fr;``` para demais navegadores.

O "filho" é responsável por escolher a sua posição:
- largura que ocupará: a forma que considero mais fácil é colocar onde inicia o elemento e quantos espaços ocupará usando ```grid-span```
  ```-ms-grid-column: 1; -ms-grid-column-span: 3;``` para IE.
  
  ```grid-column: 1 / span 3;``` para demais navegadores.

- algura que ocupará:
  ```-ms-grid-row: 1; -ms-grid-row-span: 3;``` para IE.
  
  ```grid-row: 1 / span 3;``` para demais navegadores.

O ```grid-gap:``` não pode ser usado, mas um truque muito útil é utilizar ```margin``` nos "filhos" com a metade do tamanho que deseja-se 
o afastamentos deles e então no pai com ```margin``` do tamanho do gap, mas negativo.





